/*
 * main_loop.h
 *
 * Created: 04/12/2018 10:14:43
 *  Author: Collo
 */ 


#ifndef MAIN_LOOP_H_
#define MAIN_LOOP_H_

#define STATE_INITIALISING		0
#define STATE_IDLE				1
#define STATE_SELECTION			2
#define STATE_DISPENSE			3
#define STATE_MAINTENANCE		4

typedef struct vmcSettings{
	unsigned int price_per_l;
	unsigned int maxPrice;
	unsigned int minPrice;
	float calibrateQ;
	unsigned int pulses_per_l;
}vmcSettings_t;

void vmc_init(void);
void main_loop(void);

void vmc_set_price(uint16_t);
void vmc_set_min_price(uint16_t);
void vmc_set_max_price(uint16_t);
void init_settings(void);
uint16_t get_unit_price(void);

unsigned int get_price(void);

static void idle(void);
static void selection(void);

double get_total_litres(void);
bool dispense_product(double);

void vmc_eeprom_update_settings(void);
void maintenance(void);

extern vmcSettings_t settings;
extern char machineState;

#endif /* MAIN_LOOP_H_ */