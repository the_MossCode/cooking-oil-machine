/*
 * sm_keypad.cpp
 *
 * Created: 15/02/2019 13:26:00
 *  Author: Collo
 */ 
#include "cookingmachine.h"

unsigned char keys[KEYPAD_ROWS][KEYPAD_COLUMNS] = {
	{'1','2','3','A'},
	{'4','5','6','B'},
	{'7','8','9','C'},
	{'*','0','#','D'}
};

uint8_t rowPins[4] = {A12, A13, A14, A15};
uint8_t columnPins[4] = {A8, A9, A10, A11};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, columnPins, KEYPAD_ROWS, KEYPAD_COLUMNS);

void init_keypad(unsigned int _timeout)
{
	DEBUG_PRINT("Initialising KeyPad");
	keypad.setDebounceTime(10);
}

char get_keypad_key()
{
	return keypad.getKey();
}

unsigned char get_keypad_number_key()
{
	char key = get_keypad_key();
	if(key >= 0x30 && key <= 0x39){
		key -= 0x30;
		return key;
	}
	else{
		return 0xff;
	}
}

unsigned int get_keypad_input()
{
	String input = "";
	char key = 0;
	
	lcd_clear_screen();
	lcd_print_custom_message("Input:", 0, 0);
	
	do{
		wdt_reset();
		key = get_keypad_key();
		if(key == 'C'){
			return 0;
		}
		else if(key >= '0' && key <= '9'){
			if(input.toInt() >= 0xffff){
				lcd_clear_screen();
				lcd_print_error_msg("VALUE TOO HIGH");
				lcd_clear_screen();
				lcd_print_custom_message("Input:", 0, 0);
				continue;
			}
			Serial.println(String(key));
			input += String(key);
			
			lcd_clear_screen();
			lcd_print_custom_message("Input:", 0, 0);
			lcd_print_custom_message(input.c_str(), 1, LCD_COLUMNS - input.length());
			
			DEBUG_PRINT("INPUT: " + input);
		}
	}while(key != 'D');
	
	if(input.length() <= 0){return 0;}
	else{return input.toInt();}
}