/*
 * RTC.h
 *
 * Created: 06/02/2019 16:04:04
 *  Author: Collo
 */ 
#ifndef RTC_H_
#define RTC_H_

#define DS3221					0x68

#define SECOND_REG				0x00
#define MINUTE_REG				0x01
#define HOUR_REG				0x02
#define DAY_REG					0x03
#define DATE_REG				0x04
#define MONTH_CENT_REG			0x05
#define YEAR_REG				0x06
#define CONTROL_REG				0x0e
#define CONTROL_STATUS_REG		0x0f
#define AGING_OFFSET_REG		0x10
#define TEMP_MSB_REG			0x11
#define TEMP_LSB_REG			0x12

#define RTC_DEBUG_PORT			Serial

void rtc_setup(void);
void rtc_set_year(unsigned char);
void rtc_set_month(unsigned char);
void rtc_set_date(unsigned char);
void rtc_set_day(unsigned char);
void rtc_set_hour(unsigned char);
void rtc_set_minutes(unsigned char);
void rtc_set_seconds(unsigned char);

void rtc_set_address(unsigned char);
unsigned char rtc_get_byte(unsigned char);
void rtc_send_byte(unsigned char, unsigned char);

unsigned int rtc_get_year(void);
unsigned char rtc_get_month(void);
unsigned char rtc_get_date(void);
unsigned char rtc_get_day(void);
unsigned char rtc_get_hour(void);
unsigned char rtc_get_minutes(void);
unsigned char rtc_get_seconds(void);

unsigned char get_decimal_val(unsigned char);
unsigned char dec_to_bcd(unsigned char);

void rtc_diagnostics();
void update_processor_time();
#endif /* RTC_H_ */