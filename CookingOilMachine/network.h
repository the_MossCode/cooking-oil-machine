/*
 * network.h
 *
 * Created: 21/01/2019 11:03:28
 *  Author: Collo
 */ 


#ifndef NETWORK_H_
#define NETWORK_H_

#define TINY_GSM_MODEM_SIM800
#define SerialAT Serial1

//#define GPRS_APN      "bew.orange.co.ke"
#define GPRS_APN      "safaricom"
#define GPRS_USER     "saf"
#define GPRS_PASS     "data"

// #define GPRS_APN      "internet"
// #define GPRS_USER     "guest"
// #define GPRS_PASS     ""

#include <TinyGsmClient.h>

extern TinyGsm modem;
extern TinyGsmClient client;
extern String IMEI;

bool start_comms(void);
bool gprs_connect(void);

#endif /* NETWORK_H_ */