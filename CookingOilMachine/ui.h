/*
 * ui.h
 *
 * Created: 03/12/2018 15:23:05
 *  Author: Collo
 */ 
#ifndef UI_H_
#define UI_H_

#define RS					22
#define EN					23
#define D4					24
#define D5					25
#define D6					26
#define D7					27

#define LCD_ROWS			4
#define LCD_COLUMNS			20

#define KEYPAD_ROWS			4
#define KEYPAD_COLUMNS		4

enum screen{
	SCREEN_IDLE, SCREEN_PRICE, SCREEN_SELECTION, SCREEN_DISPENSING, SCREEN_DISPENSE_STATUS, SCREEN_MENU
};

void init_display();
void ui_update(uint8_t);
void lcd_print_custom_message(const char *, uint8_t, uint8_t);
void lcd_clear_screen();
void lcd_print_error_msg(const char*);
void set_current_screen(uint8_t);
uint8_t get_current_screen();

void update_lcd_message(const char*, uint8_t, uint8_t);
uint8_t get_message_length(const char*);
void lcd_write_char_array(char *);
void print_price_message(const char*);
void print_selection_message(const char*, const char*);

void lcd_update_price_menu(char);

void lcd_update_time();
void lcd_audit_handler(unsigned char);

static float get_ml(uint16_t);

extern screen uiScreen;

#endif /* UI_H_ */