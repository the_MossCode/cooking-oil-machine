/*
 * sm_keypad.h
 *
 * Created: 15/02/2019 13:24:12
 *  Author: Collo
 */ 


#ifndef SM_KEYPAD_H_
#define SM_KEYPAD_H_

#include "Keypad.h"

extern Keypad keypad;

char get_keypad_key();
unsigned char get_keypad_number_key();
unsigned int get_keypad_input();
void init_keypad(unsigned int);


#endif /* SM_KEYPAD_H_ */