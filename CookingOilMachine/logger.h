/*
 * logger.h
 *
 * Created: 07/02/2019 15:05:42
 *  Author: Collo
 */ 


#ifndef LOGGER_H_
#define LOGGER_H_

#define LOGGER_YEAR_ADDRESS				30
#define LOGGER_MONTH1_ADDRESS			32
#define LOGGER_MONTH2_ADDRESS			34
#define LOGGER_MONTH3_ADDRESS			36
#define LOGGER_MONTH4_ADDRESS			38
#define LOGGER_MONTH5_ADDRESS			40
#define LOGGER_MONTH6_ADDRESS			42
#define LOGGER_MONTH7_ADDRESS			44
#define LOGGER_MONTH8_ADDRESS			46
#define LOGGER_MONTH9_ADDRESS			48
#define LOGGER_MONTH10_ADDRESS			50
#define LOGGER_MONTH11_ADDRESS			52
#define LOGGER_MONTH12_ADDRESS			54

#define LOGGER_DAY1_ADDRESS				60
#define LOGGER_DAY2_ADDRESS				63
#define LOGGER_DAY3_ADDRESS				66
#define LOGGER_DAY4_ADDRESS				69
#define LOGGER_DAY5_ADDRESS				72
#define LOGGER_DAY6_ADDRESS				75
#define LOGGER_DAY7_ADDRESS				78


void add_log(unsigned int);
void get_log(unsigned int *, unsigned char*, unsigned char);

void update_day_log(unsigned int );
void update_month_log(void);

unsigned int get_day_log(unsigned char, unsigned char*);

#endif /* LOGGER_H_ */