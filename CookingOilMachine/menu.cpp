/*
 * menu.cpp
 *
 * Created: 07/02/2019 13:32:54
 *  Author: Collo
 */ 
#include "menu.h"
#include "cookingmachine.h"

Menu menu = MENU_SET_PRICES;
SubMenu calibrateMenu = CALIBRATE_TOP_LEVEL;

char menuPin[4] = {'0','0','0','0'};

void menu_handler()
{
	wdt_reset();
	switch(menu){
		case MENU_SET_PRICES:{
			lcd_clear_screen();
			update_lcd_message("SET PRICES", 0, 0);
			update_lcd_message("Press 'D'", 1, 0);
			break;
		}
// 		case MENU_SET_MIN:{
// 			lcd_clear_screen();
// 			update_lcd_message("SET MIN PRICE", 0, 0);
// 			//update_lcd_message("Press 'D' to input value", 1, 0);
// 			break;
// 		}
// 		case MENU_SET_MAX:{
// 			lcd_clear_screen();
// 			update_lcd_message("SET MAX PRICE", 0, 0);
// 			//update_lcd_message("Press 'D' to input value", 1, 0);
// 			break;
// 		}
		case MENU_CALIBRATE:{
			calibrate_menu_handler();
			break;
		}
		case MENU_TIME:{
			lcd_update_time();
			break;
		}
		case MENU_AUDIT:{
			break;
		}
	}
}

void calibrate_menu_handler()
{
	wdt_reset();
	switch(calibrateMenu){
		case CALIBRATE_TOP_LEVEL:{
			lcd_clear_screen();
			update_lcd_message("CALIBRATE", 0, 0);
			update_lcd_message("(Press 'D')", 1, 0);
			break;
		}
		case CALIBRATE_SET_ML:{
			lcd_clear_screen();
			update_lcd_message("QUANTITY:", 0, 0);
			break;
		}
		case CALIBRATE_DISPENSE:{
			lcd_clear_screen();
			update_lcd_message("Dispensing...", 0, 0);
			break;
		}
		case CALIBRATE_SET_PULSES:{
			lcd_clear_screen();
			//update_lcd_message("ENTER DISPENSED LITRES:", 0, 0);
			break;
		}
		default:{
			break;
		}
	}
}

void maintenance_loop()
{
	char currMenu = menu;
	char key = 0;
	
	update_menu(currMenu);
	do{
		wdt_reset();
		key = get_keypad_key();
		
		switch(key){
			case '4':{
				if(--currMenu > MENU_TIME){currMenu = MENU_TIME;}
				update_menu(currMenu);
				break;
			}
			case '6':{
				if(++currMenu > MENU_TIME){currMenu = MENU_SET_PRICES;}
				update_menu(currMenu);
				break;
			}
			case '#':{
				exit_menu();
				return;
			}
		}
		
		switch(currMenu){
			case MENU_SET_PRICES:{
				if(key == 'D'){menu_set_prices();}
				break;
			}
			case MENU_TIME:{
				menu_set_time();
				break;
			}
			case MENU_CALIBRATE:{
				if(key == 'D'){calibration_loop();}
				break;
			}
		}	
	}while(key != 'C');
}

void exit_menu()
{
	lcd_clear_screen();
	lcd_print_custom_message("Please Wait...", 0, 0);
	vmc_eeprom_update_settings();
	machineState = STATE_IDLE;
	ui_update(SCREEN_IDLE);
}

void menu_set_prices()
{
	char key = 0, priceMenu = 0;
	
	lcd_update_price_menu(priceMenu);
	do{
		wdt_reset();
		key = get_keypad_key();
		
		switch(key){
			case '4':{
				if(--priceMenu < 0){priceMenu = 2;}
				lcd_update_price_menu(priceMenu);
				break;
			}
			case '6':{
				if(++priceMenu >= 3){priceMenu = 0;}
				lcd_update_price_menu(priceMenu);
				break;
			}
			case '2':{
				update_price_settings(priceMenu, 0, key);
				lcd_update_price_menu(priceMenu);
				break;
			}
			case '8':{
				update_price_settings(priceMenu, 0, key);
				lcd_update_price_menu(priceMenu);
				break;
			}
			case '#':{
				return;
			}
			case 'D':{
				unsigned int inputPrice = get_keypad_input();
				if(inputPrice > 0){update_price_settings(priceMenu, inputPrice, 0);}
				lcd_update_price_menu(priceMenu);
			}
			default:{
				if(key){Serial.println(key);}
			}
		}
	}while(key !='C');
}

void menu_set_time(){
	char key = 0;
	
	lcd_update_time();
}

char set_prices_loop()
{
	char key = 0, currMenu = menu;
	unsigned int tempPrice = settings.price_per_l;
// 	if(currMenu == MENU_SET_MIN){tempPrice = settings.minPrice;}
// 	else if(currMenu == MENU_SET_MAX){tempPrice = settings.maxPrice;}
	lcd_print_custom_message((String(tempPrice)).c_str(), 1, LCD_COLUMNS - (String(tempPrice)).length());
	do{
		wdt_reset();
		key = get_keypad_key();
		if(key){Serial.println(key);}
		switch(key){
			case '2':{
				tempPrice--;
				lcd_clear_screen();
				ui_update(SCREEN_MENU);
				lcd_print_custom_message((String(tempPrice)).c_str(), 1, LCD_COLUMNS - (String(tempPrice)).length());
				break;
			}
			case '4':{
				//if(tempPrice){update_price_settings(tempPrice);}
				menu = --currMenu;
				return 0;
			}
			case '6':{
				//if(tempPrice){update_price_settings(tempPrice);}
				menu = ++currMenu;
				return 0;
			}
			case '8':{
				tempPrice++;
				lcd_clear_screen();
				ui_update(SCREEN_MENU);
				lcd_print_custom_message((String(tempPrice)).c_str(), 1, LCD_COLUMNS - (String(tempPrice)).length());
				break;
			}
			case '#':{
				tempPrice++;
				lcd_clear_screen();
				ui_update(SCREEN_MENU);
				//if(tempPrice){update_price_settings(tempPrice);}
				return 1;
			}
			case 'D':{
				tempPrice = get_keypad_input();
				break;
			}
			default:{;}
		}
	}while(key |= 'C');
	return 0;
}

void calibration_loop()
{
	wdt_reset();
	char key = 0;
	char currSubMenu = get_calibrate_menu();
	
	update_menu(MENU_CALIBRATE);
	while(key != 'C'){
		wdt_reset();
		key = get_keypad_key();
		currSubMenu = get_calibrate_menu();
		
		switch(currSubMenu){
			case CALIBRATE_TOP_LEVEL:{
				if(key == 'D'){
					set_calibrate_menu(CALIBRATE_SET_ML);
					update_menu(MENU_CALIBRATE);
				}
				break;
			}
			case CALIBRATE_SET_ML:{
				if(key == '2'){settings.calibrateQ += 0.1;}
				else if(key == '8'){settings.calibrateQ -= 0.1;}
				else if(key == '6' || key == 'D'){
					set_calibrate_menu(CALIBRATE_DISPENSE);
					update_menu(MENU_CALIBRATE);
					break;
				}
				
				lcd_print_custom_message((String(settings.calibrateQ, 2)).c_str(), 1, LCD_COLUMNS - (String(settings.calibrateQ, 2)).length());
				break;
			}
			case CALIBRATE_DISPENSE:{
				//litres = settings.calibrateQ;
				dispense_product(settings.calibrateQ);//ToDo, Dispense takes the number of liters as a parameter;
				set_calibrate_menu(CALIBRATE_SET_PULSES);
				update_menu(MENU_CALIBRATE);
				break;
			}
			case CALIBRATE_SET_PULSES:{
				unsigned int tempData = 0;
					lcd_clear_screen();
					lcd_print_error_msg("Enter Dispensed L:");
					wdt_reset();
					key = get_keypad_key();
					if(key == 'C'){
						set_calibrate_menu(CALIBRATE_TOP_LEVEL);
						update_menu(MENU_CALIBRATE);
						return;
					}
					else if(key >= '0' && key <= '9'){
						tempData = get_keypad_input();
						while(tempData <= 0){
							wdt_reset();
							lcd_clear_screen();
							lcd_print_error_msg("No Input");
							tempData = get_keypad_input();
						}
					}
				settings.pulses_per_l = calculate_pulse_per_litre(tempData, get_pulse_count());
				lcd_clear_screen();
				lcd_print_error_msg("OK");
				set_calibrate_menu(CALIBRATE_TOP_LEVEL);
				menu = MENU_CALIBRATE;
				return;
// 				if(key == '8'){set_pulses_per_litre(get_pulses_per_litre() - 1);}
// 				else if(key == '2'){set_pulses_per_litre(get_pulses_per_litre() + 1);}
// 				else if(key == '6'){
// 					set_calibrate_menu(CALIBRATE_SET_ML);
// 					update_menu(MENU_CALIBRATE);
// 					break;
// 				}
// 				else if(key == 'D'){
// 					set_calibrate_menu(CALIBRATE_TOP_LEVEL);
// 					eeprom_update_word((uint16_t*)PULSE_ADDRESS ,get_pulses_per_litre());
// 					return;
// 				}
// 				//else{break;}
// 				
// 				lcd_print_custom_message((String(get_pulses_per_litre())).c_str(), 1, LCD_COLUMNS - (String(get_pulses_per_litre()).length()));
				break;
			}
			default:{break;}
		}
	}
	
	set_calibrate_menu(CALIBRATE_TOP_LEVEL);
}

void time_menu_loop()
{
	wdt_reset();
	char key = get_keypad_key();
	unsigned char currentTimeVar = 0;
	//uint32_t blinkMillis = millis();
	do{
		wdt_reset();
		switch(key){
			case 'D':{
				currentTimeVar++;
				if(currentTimeVar > 5){currentTimeVar = 0;}
				break;
			}
			case '2':{
				switch(currentTimeVar){
					case 0:{
						rtcYear++;
						break;
					}
					case 1:{
						rtcMonth++;
						break;
					}
					case 2:{
						rtcDate++;
						break;
					}
					case 3:{
						rtcHour++;
						break;
					}
					case 4:{
						rtcMinute++;
						break;
					}
					case 5:{
						rtcSecond++;
						break;
					}
					default:{break;}
				}
				lcd_update_time();
				break;
			}
			case '8':{
				switch(currentTimeVar){
					case 0:{
						rtcYear--;
						break;
					}
					case 1:{
						rtcMonth--;
						break;
					}
					case 2:{
						rtcDate--;
						break;
					}
					case 3:{
						rtcHour--;
						break;
					}
					case 4:{
						rtcMinute--;
						break;
					}
					case 5:{
						rtcSecond--;
						break;
					}
					default:{break;}
				}
				lcd_update_time();
				break;
			}
			default:{
				//DEBUG_PRINT(String(key));
				break;
			}
		}
		key = get_keypad_key();
	}while(key != 'C');
	
	rtc_set_year(rtcYear - 2000);
	rtc_set_month(rtcMonth);
	rtc_set_date(rtcDate);
	rtc_set_hour(rtcHour);
	rtc_set_minutes(rtcMinute);
	rtc_set_seconds(rtcSecond);
	
	set_current_menu(MENU_AUDIT);
}

bool is_menu_pin_input_ok()
{
	wdt_reset();
	char tempPin[4];
	char key;
	uint8_t retries=0, pinptr=0;
	uint32_t timeout = millis();
	bool success = false;
	
	lcd_clear_screen();
	lcd_print_custom_message("ENTER PIN: ", 0, 0);
	
	while(!success){
		wdt_reset();
		if(millis() - timeout > 120000 || retries > 2){
			if(retries > 2){lcd_print_error_msg("Too Many Attempts");_delay_ms(1000);}
			break;
		}
		
		key = get_keypad_key();
		
		if(key){
			if(key == 'C'){
				if(pinptr > 0){
					lcd_clear_screen();
					lcd_print_custom_message("ENTER PIN: ", 0, 0);
					pinptr = 0;
					continue;
				}
				else{
					lcd_clear_screen();
					ui_update(SCREEN_IDLE);
					return false;
				}
			}
			else if(key == '#'){return false;}
			DEBUG_PRINT(key);
			tempPin[pinptr++] = key;
			lcd_print_custom_message("*", 1, pinptr);
		}
		
		if(pinptr >= 4){
			retries++;
			pinptr = 0;
			for(uint8_t i=0; i<4; ++i){
				if(tempPin[i] != menuPin[i]){
					lcd_print_error_msg("WRONG PIN");
					lcd_clear_screen();
					_delay_ms(100);
					lcd_print_custom_message("ENTER PIN: ", 0, 0);
					timeout = millis();
					break;
				}
				else if(i==3){lcd_clear_screen(); success = true;}
			}
		}
	}
	if(success){DEBUG_PRINT("OK Pin");}
	else{DEBUG_PRINT("Timeout");}
	return success;
}


void audit_menu_loop()
{
	wdt_reset();
	menu = MENU_AUDIT;
	
	char key = get_keypad_key();
	unsigned char _day = 1;
	
	lcd_clear_screen();
	lcd_print_custom_message("AUDIT, Please Wait", 0, 0);
	lcd_audit_handler(_day);
	
	do{
		wdt_reset();
		switch(key){
			case 'D':{
				return;
			}
			case '6':{
				if(++_day > 7){
					_day = 1;
				}
			lcd_clear_screen();
			lcd_print_custom_message("Wait..", 0, 0);
				lcd_audit_handler(_day);
				break;
			}
			case '4':{
				if(--_day > 7){
					_day = 1;
				}
			lcd_clear_screen();
			lcd_print_custom_message("Wait..", 0, 0);
				lcd_audit_handler(_day);
				break;
			}
			default:{
				break;
			}
		}
		key = get_keypad_key();
	}while(key != 'C');
}

//Getters and Setters
void set_current_menu(unsigned char _menu)
{
	menu = _menu;
}

unsigned char get_current_menu()
{
	return menu;
}

void update_menu(unsigned char _menu)
{
	set_current_menu(_menu);
	menu_handler();
}

unsigned char get_calibrate_menu()
{
	return calibrateMenu;
}

void set_calibrate_menu(unsigned char _cm)
{
	calibrateMenu = _cm;
}

static void update_price_settings(char _menu, unsigned int _price = NULL, char _key = NULL)
{
	if(_key == '8'){
		if(_menu == 0){//Set Price Per Litre
			settings.price_per_l--;
		}
		else if(_menu == 1){//Set Min Price
			settings.minPrice--;
		}
		else if(_menu == 2){// Set Max Price
			settings.maxPrice--;
		}		
	}
	else if(_key == '2'){
		if(_menu == 0){//Set Price Per Litre
			settings.price_per_l++;
		}
		else if(_menu == 1){//Set Min Price
			settings.minPrice++;
		}
		else if(_menu == 2){// Set Max Price
			settings.maxPrice++;
		}		
	}
	else{
		if(_menu == 0){//Set Price Per Litre
			settings.price_per_l = _price;
		}
		else if(_menu == 1){//Set Min Price
			settings.minPrice = _price;
		}
		else if(_menu == 2){// Set Max Price
			settings.maxPrice = _price;
		}
	}
}