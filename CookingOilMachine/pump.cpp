/*
 * pump.cpp
 *
 * Created: 16/01/2019 14:05:36
 *  Author: Collo
 */ 
#include "cookingmachine.h"

void init_pump()
{
	pinMode(PUMP_PIN, OUTPUT);
	digitalWrite(PUMP_PIN, LOW);
	pinMode(SOLENOID_VALVE, OUTPUT);
	digitalWrite(SOLENOID_VALVE, LOW);
}

void start_pump()
{
	digitalWrite(PUMP_PIN, HIGH);
	_delay_ms(500);
}

void stop_pump()
{
	digitalWrite(PUMP_PIN, LOW);
}