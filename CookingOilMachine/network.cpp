/*
 * network.cpp
 *
 * Created: 21/01/2019 11:19:59
 *  Author: Collo
 */ 

#include "cashless.h"

//int CONNECTION_STATE=0;

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);

String IMEI = "";

bool start_comms()
{
	DEBUG_PRINT("START COMMUNICATIONS");
	wdt_disable();
	
	SerialAT.begin(115200);
	_delay_ms(1000);
	
	//*********************
	pinMode(28, OUTPUT);
	digitalWrite(28, HIGH);
	_delay_ms(2000);
	//**********************
	
	if(modem.restart()){
		_delay_ms(100);
		DEBUG_PRINT("[OK] MODEM");
	}
	
	gprs_connect();
	IMEI = modem.getIMEI();
	//IMEI = "867856034144409";
	DEBUG_PRINT(IMEI);
	
	mqtt.begin(MQTT_SERVER, client);
	mqtt.setOptions(120, false, 5000);//KeepAlive 60->120
	mqtt.onMessage(messageReceived);
	mqtt_subscribe();
	delay(1000);
	#ifdef TINY_GSM_MODEM_HAS_GPS
	modem.enableGPS();
	#endif
	_delay_ms(500);
	wdt_enable(WDTO_8S);
}

/*@return true if the device can connects.
*/
bool gprs_connect()
{
	while(SerialAT.available()>0){
		Serial.println(SerialAT.read());
	}

	wdt_disable();
	int gprsConnectionAttempts=0;

	DEBUG_PRINT("[WAIT] NETWORK");

	uint32_t timeout = millis();
	if(!modem.isNetworkConnected()){
		while (!modem.waitForNetwork(10000)){
			DEBUG_PRINT("[WAIT]");
			if(++gprsConnectionAttempts%2 == 0){
				DEBUG_PRINT("MODEM RESTART");
				modem.restart();
			}
			delay(250);
			if((millis() - timeout) > 60000){
				DEBUG_PRINT("Unable to Establish Network Connection");
				
				#ifdef MDB_AUDIT_DEVICE
				write_state_to_eeprom(get_user_funds());
				#endif
				
				DEBUG_PRINT("REBOOT");
				wdt_enable(WDTO_120MS);//Shorter WatchDog Reset For Reboot
				while(1){;}
			}
		}
		gprsConnectionAttempts = 0;
	}
	//}

	DEBUG_PRINT("[OK] NETWORK");
	DEBUG_PRINT("[WAIT] GPRS");
	while (!modem.gprsConnect(GPRS_APN, GPRS_USER/*, GPRS_PASS*/)){
		if(gprsConnectionAttempts++ > 10){
			DEBUG_PRINT("REBOOT");
			
			#ifdef MDB_AUDIT_DEVICE
			write_state_to_eeprom(get_user_funds());
			#endif
			
			wdt_enable(WDTO_120MS);
			while(1){;}
		}
		delay(1000);
		DEBUG_PRINT(".");
	}
	gprsConnectionAttempts=0;
	wdt_enable(WDTO_8S);
	DEBUG_PRINT("[OK] GPRS");
	//CONNECTION_STATE = CONNECTION_STATE_GPRS;
	return true;
}