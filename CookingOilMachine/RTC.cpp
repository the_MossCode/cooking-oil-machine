/*
 * RTC.cpp
 *
 * Created: 06/02/2019 16:33:57
 *  Author: Collo
 */ 

#include "cookingmachine.h"
#include <Wire.h>

//TwoWire rtc = TwoWire();

#define rtc Wire

unsigned int rtcYear = 0;
unsigned char rtcMonth = 0;
unsigned char rtcDate = 0;
unsigned char rtcHour = 0;
unsigned char rtcMinute = 0;
unsigned char rtcSecond = 0;
unsigned char rtcDay = 0;

void rtc_setup()
{
	rtc.begin();
	_delay_ms(10);
	update_processor_time();
	RTC_DEBUG_PORT.println("Date: " + String(rtcDate) + "/" + String(rtcMonth) + "/" + String(rtcYear));
	RTC_DEBUG_PORT.println("Time: " + String(rtcHour) + ":" + String(rtcMinute) + "/" + String(rtcSecond));
}
unsigned char get_decimal_val(unsigned char bcdVal)
{
// 	unsigned char test = bcdVal & 0x0f;
// 	Serial.println("TEST CHAR 1: " + String(test));
// 	test = (bcdVal>>4) & 0x0f;
// 	Serial.println("TEST CHAR 2: " + String(test));
	if((bcdVal&0x0f) >= 10){return 0xff;}
	else if(((bcdVal>>4)&0x0f) >= 10){return 0xff;}
	else{
		unsigned char dec = 0;
		dec |= (bcdVal & 0x0f);
		bcdVal >>= 4;
		dec += ((bcdVal & 0x0f) * 10);
		return dec;
	}
}

unsigned char dec_to_bcd(unsigned char _data)
{
	unsigned char tens = _data/10;
	//Serial.println("TENS: " + String(tens));
	unsigned char ones = _data - (tens*10);
	//Serial.println("ONES: " + String(ones));
	unsigned char bcd = 0;
	bcd |= tens;
	bcd <<= 4;
	bcd |= ones;
	return bcd;
}

void rtc_set_address(unsigned char _address)
{
	rtc.write(_address);
}

//Getters
unsigned int rtc_get_year()
{
	wdt_reset();
	rtc.beginTransmission(DS3221);
	rtc_set_address(MONTH_CENT_REG);
	rtc.endTransmission();
	_delay_ms(50);
	rtc.requestFrom(DS3221, 2);
	
	uint32_t timeout = millis();
	while(rtc.available() < 2){
		wdt_reset();
		if(millis() - timeout > 1000){
			return 0;
		}
	}
	unsigned char c = rtc.read();
	//Serial.println(c);
	if(c & 0b10000000){return 0;}
	else{
		c = rtc.read();
	}
	//Serial.println(c);
	
	return 2000 + get_decimal_val(c);
}

unsigned char rtc_get_byte(unsigned char _register)
{
	wdt_reset();
	rtc.beginTransmission(DS3221);
	rtc_set_address(_register);
	rtc.endTransmission();
	_delay_ms(50);
	rtc.requestFrom(DS3221, 1);
	
	uint32_t timeout = millis();
	while(!rtc.available()){
		if(millis() - timeout > 500){return 0xff;}	
	}
	unsigned char c = rtc.read();
	Serial.println(c, HEX);
	return c;
}

void rtc_send_byte(unsigned char _register, unsigned char _data)
{
	wdt_reset();
	rtc.beginTransmission(DS3221);
	rtc_set_address(_register);
	rtc.write(_data);
	rtc.endTransmission();
	_delay_ms(50);
}

unsigned char rtc_get_month()
{
	DEBUG_PRINT("\nMONTH");
	unsigned char c = rtc_get_byte(MONTH_CENT_REG);
	c &= 0b01111111;
	return get_decimal_val(c);
}

unsigned char rtc_get_date()
{
	DEBUG_PRINT("\nDATE");
	unsigned char c = rtc_get_byte(DATE_REG);
	return get_decimal_val(c);	
}

unsigned char rtc_get_day()
{
	DEBUG_PRINT("\nDAY");
	unsigned char c = rtc_get_byte(DAY_REG);
	return get_decimal_val(c);	
}

unsigned char rtc_get_hour()
{
	DEBUG_PRINT("\nHOUR");
	unsigned char c = rtc_get_byte(HOUR_REG);
	c &= 0b00111111;
	return get_decimal_val(c);
}

unsigned char rtc_get_minutes()
{
	DEBUG_PRINT("\nMINUTE");
	unsigned char c = rtc_get_byte(MINUTE_REG);
	return get_decimal_val(c);
}

unsigned char rtc_get_seconds()
{
	DEBUG_PRINT("\nSECOND");
	unsigned char c = rtc_get_byte(SECOND_REG);
	return get_decimal_val(c);
}

//Setters
void rtc_set_year(unsigned char _year)
{
	//unsigned char yearChar = _year & 0xff;
	//Serial.println(yearChar);
	rtc_send_byte(YEAR_REG, dec_to_bcd(_year));
}

void rtc_set_month(unsigned char _month)
{
	rtc_send_byte(MONTH_CENT_REG, dec_to_bcd(_month));
}

void rtc_set_date(unsigned char _date)
{
	rtc_send_byte(DATE_REG, dec_to_bcd(_date));
}

void rtc_set_day(unsigned char _day)
{
	rtc_send_byte(DAY_REG, dec_to_bcd(_day));
}

void rtc_set_hour(unsigned char _hour)
{
	rtc_send_byte(HOUR_REG, dec_to_bcd(_hour));
}

void rtc_set_minutes(unsigned char _minutes)
{
	rtc_send_byte(MINUTE_REG, dec_to_bcd(_minutes));
}

void rtc_set_seconds(unsigned char _seconds)
{
	rtc_send_byte(SECOND_REG, dec_to_bcd(_seconds));
}

void update_processor_time()
{
	rtcYear = rtc_get_year();
	rtcMonth = rtc_get_month();
	rtcDate = rtc_get_date();
	rtcDay = rtc_get_day();
	rtcHour = rtc_get_hour();
	rtcMinute = rtc_get_minutes();
	rtcSecond = rtc_get_seconds();
}