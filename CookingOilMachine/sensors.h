/*
 * sensors.h
 *
 * Created: 10/12/2018 10:42:06
 *  Author: Collo
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#define FLOW_METER

#ifdef FLOW_METER

#ifdef __cplusplus
extern "C"{
#endif

//Flow meter is linear below 600l/h(10l/min) keep speed below this.
#define FLOW_METER_M			(80/11)
#define FLOW_METER_C			(40/11)
#define COUNTER_F				(F_CPU/1024)// divide by prescaler	

void init_flow_meter();
void init_temperature_sensor();

double get_flow_meter_frequency();
double get_flow_rate();
double get_flow_ml();
void clear_pulse_count();
unsigned int get_pulse_count();
uint16_t get_pulses_per_litre();
void set_pulses_per_litre(uint16_t);
unsigned int calculate_pulse_per_litre(float, unsigned int);

float get_current_temperature();

#ifdef __cplusplus
}
#endif

#endif

#endif /* SENSORS_H_ */