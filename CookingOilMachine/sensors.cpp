/*
 * sensors.cpp
 *
 * Created: 10/12/2018 10:42:22
 *  Author: Collo
 */ 

#include "cookingmachine.h"
#include "OneWire.h"
#include "DallasTemperature.h"
#include <avr/interrupt.h>

#ifdef FLOW_METER

//FLOW METER GIVES 450 PULSES FOR EVERY LITRE
//THERFORE 1 PULSE = 1000/450 = 2.2222ML
//COUNT THE PULSES?

#ifdef __cplusplus
extern "C"{
#endif

volatile int meterPulseCounter = 0;
volatile uint16_t meterPulsePeriod[10];
double meterFrequency = 0;
double flowRate = 0;

volatile unsigned int pulseCount = 0;
unsigned int pulses_per_litre = 450;

void init_flow_meter()
{
	pulses_per_litre = eeprom_read_word((uint16_t*)PULSE_ADDRESS);
	
	//Init Input Capture
	TCCR5A = 0x00;
	TCCR5B = 0b11000000;
	TIMSK5 = 0b00100001;//Enable Input Capture and Overflow Interrupts
	
	sei();//Enable Interrupts 
	TCCR5B |= (1<<CS52) | (1<<CS50);//Start Counter, 256 prescaler
}

double get_flow_ml()
{
	double flowML = 0;
	
	flowML = double(pulseCount) * (1000/pulses_per_litre);
	
 	//DEBUG_PRINT("Pulse Count: "+ String(pulseCount));
	//DEBUG_PRINT("ml" + String(flowML));
	
	return flowML;
}

uint16_t get_pulses_per_litre()
{
	return pulses_per_litre;
}

void set_pulses_per_litre(uint16_t _p)
{
	pulses_per_litre = _p;
}

void clear_pulse_count()
{
	pulseCount = 0;
}

unsigned int get_pulse_count()
{
	return pulseCount;
}

unsigned int calculate_pulse_per_litre(float _litres, unsigned int _pulseCount)
{
	unsigned int tempPulses = 0;
	tempPulses = _pulseCount/_litres;
	return tempPulses;
}

double get_flow_meter_frequency()
{
	double averagePulsePeriodCount = 0;
	if(meterPulseCounter >= 10){
		double countTotal = 0;
		for(uint8_t i=0; i<meterPulseCounter; ++i){
			countTotal += (double)meterPulsePeriod[i];
		}
		averagePulsePeriodCount = countTotal/(double)meterPulseCounter;
		
		meterPulseCounter = 0;
	}
	
	if(averagePulsePeriodCount == 0){return 0;}
	DEBUG_PRINT("Counter Val:" + String(averagePulsePeriodCount));
	double T = averagePulsePeriodCount / COUNTER_F;
	double f = 1/T;
	DEBUG_PRINT("Frequency:" + String(f));
	return f;
}

double get_flow_rate()
{
	double flowRate = 0;
	double f = get_flow_meter_frequency();
	if(f==0){return 0;}
	flowRate += (f*80) + 40;
	flowRate /= 11;
	DEBUG_PRINT("Flow Rate: " + String(flowRate));
	
	return flowRate;
}

ISR(TIMER5_CAPT_vect)
{
	uint16_t counterVal = 0;
	
	counterVal |= ICR5L;
	counterVal <<= 8;
	counterVal |= ICR5H;
	
	meterPulsePeriod[meterPulseCounter++] = counterVal;
	
	pulseCount++;
	
	TCNT5H = 0x00;//Reset Counter
	TCNT5L = 0x00;
}

ISR(TIMER5_OVF_vect)
{
	TCNT5H = 0x00;
	TCNT5L = 0x00;
}

#ifdef __cplusplus
}
#endif
#endif

#define ONE_WIRE_PIN  51
OneWire tempSensorComm(ONE_WIRE_PIN);
DallasTemperature tempSensor(&tempSensorComm);

unsigned char oneWireDevices[5];

void init_temperature_sensor()
{
	DEBUG_PRINT("Initialising Temperature Sensor");
	tempSensor.begin();
	_delay_ms(10);
	tempSensorComm.reset_search();
	_delay_ms(10);
	tempSensorComm.target_search(0x28);
	_delay_ms(10);
	
	uint32_t timeout = millis();
	do{
		if(millis() - timeout > 3000){
			DEBUG_PRINT("SENSOR ERROR");
			return;
		}
	}while(tempSensorComm.search(oneWireDevices) <= 0);
	
	DEBUG_PRINT("OK [Temperature Sensor]");
}

float get_current_temperature()
{
	DEBUG_PRINT("Reading Temperature");
	tempSensor.requestTemperatures();
	_delay_ms(10);
	return tempSensor.getTempCByIndex(0);
}