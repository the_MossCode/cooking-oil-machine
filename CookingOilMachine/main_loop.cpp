/*
 * main_loop.cpp
 *
 * Created: 04/12/2018 10:15:41
 *  Author: Collo
 */ 
#include "cookingmachine.h"

vmcSettings_t settings = {10, 1000, 20, 0.1};
char machineState = STATE_INITIALISING;
uint16_t input_price = 0;

//double litres = 0;

uint32_t selectionTimeout = millis();

void vmc_init()
{
	machineState = STATE_INITIALISING;
		
	init_display();
	init_keypad(10);
	init_flow_meter();
	init_settings();
	init_pump();
	
	input_price = 0;
	
	machineState = STATE_IDLE;
	ui_update(SCREEN_IDLE);
}

void main_loop()
{
	wdt_reset();
	switch(machineState){
		case STATE_INITIALISING:{
			break;//reboot	
		}
		case  STATE_IDLE:{//Wait For Button Press
			idle();
			break;
		}
		case STATE_SELECTION:{
			selection();
			break;
		}
		case STATE_DISPENSE:{
			double litres = (double)get_price()/(double)settings.price_per_l;
			dispense_product(litres);
			break;
		}
		case STATE_MAINTENANCE:{
			maintenance();
			break;
		}
		default:{break;}
	}
}

static void idle()
{
	String selectionStr = String(input_price);
	
	char key = get_keypad_key();
	
	switch(key){
		case 'A':{
			DEBUG_PRINT(String(key));
			break;
		}
		case 'B':{
			DEBUG_PRINT(String(key));
			break;
		}
		case 'C':{//Cancel
			DEBUG_PRINT(String(key));
			input_price = 0;
			ui_update(SCREEN_IDLE);
			break;
		}
		case 'D':{//Enter
			if(input_price >= settings.minPrice && input_price <= settings.maxPrice){
				machineState = STATE_SELECTION;
				ui_update(SCREEN_SELECTION);		
				selectionTimeout = millis();
			}
			else{
				lcd_print_error_msg("Invalid Price..");
				DEBUG_PRINT("Invalid Selection");
				input_price = 0;
				ui_update(SCREEN_IDLE);
			}
			break;
		}
		case '*':{
			DEBUG_PRINT(String(key));
			break;
		}
		case '#':{
			if(is_menu_pin_input_ok()){
				ui_update(SCREEN_MENU);	
				//maintenance_loop(0x01);
				machineState = STATE_MAINTENANCE;
			}
			DEBUG_PRINT(String(key));
			break;
		}
		case 0:{//Invalid Data
			break;
		}
		default:{//A number has been pressed.
			selectionStr += String(key);
			DEBUG_PRINT(selectionStr);
			
			if((selectionStr.toInt()) > 65000){return;}
			input_price = selectionStr.toInt();
			ui_update(SCREEN_PRICE);
			break;
		}
	}
}

static void selection()
{
	wdt_reset();
	char key = get_keypad_key();
	
	if(key == 'D'){
		DEBUG_PRINT("Dispensing");
		machineState = STATE_DISPENSE;
		ui_update(SCREEN_DISPENSING);
	}
	else if(key == 'C'){
		machineState  = STATE_IDLE;
		input_price = 0;
		ui_update(SCREEN_IDLE);
	}
	else{
		if((millis() - selectionTimeout) >= 120000){//Timeout -> 2 minutes
			machineState = STATE_IDLE;
			input_price = 0;
			lcd_print_error_msg("Timeout");
			ui_update(SCREEN_IDLE);
			return;
		}
	}
}

bool dispense_product(double _litres)
{
	wdt_reset();
	//ToDo Actual Dispensing and Feedback
	double dispensedLitres = 0;
	/*double litersToDispense = _litres;*/
	
	lcd_print_custom_message((String(_litres, 2)).c_str(), 0, LCD_COLUMNS - (String(_litres, 2)).length());	
	//
	uint32_t interval = millis();
	
	float prevDispensedLitres = 0;
	
	//ToDo: Add timeout to detect errors
	digitalWrite(SOLENOID_VALVE, HIGH);
	_delay_ms(2000);
	uint32_t timeout = millis();
	start_pump();
	clear_pulse_count();
	do{
		wdt_reset();
		dispensedLitres = get_flow_ml() / 1000;
// 		if(millis() - interval > 100){
// 			stop_pump();
// 			DEBUG_PRINT(dispensedLitres);
// 			interval = millis();
// 			lcd_print_custom_message((String(dispensedLitres, 2)).c_str(), 3, LCD_COLUMNS - (String(dispensedLitres, 2)).length());
// 			start_pump();	
// 		}
		if(millis() - timeout > 3000){
			if(dispensedLitres <= (prevDispensedLitres)){
				stop_pump();
					_delay_ms(1000);
					digitalWrite(SOLENOID_VALVE, LOW);
				input_price = 0;
				lcd_print_error_msg("ERROR DISPENSING..");
				machineState = STATE_IDLE;
				ui_update(SCREEN_IDLE);
				return false;
			}
			else{
				prevDispensedLitres = dispensedLitres;
				timeout = millis();
			}
		}
	}while(dispensedLitres < (_litres-0.05));
	stop_pump();
	_delay_ms(1000);
	digitalWrite(SOLENOID_VALVE, LOW);
	DEBUG_PRINT("Dispensed L: " + String(dispensedLitres));
	
	_delay_ms(500);
	lcd_print_error_msg("SUCCESS...");
	add_log(input_price);
	
	input_price = 0;
	lcd_clear_screen();
	machineState = STATE_IDLE;
	ui_update(SCREEN_IDLE);
	return true;
}

void maintenance()
{
	wdt_reset();
	char key = get_keypad_key();
	String inputStr = "";
	uint8_t currMenu = get_current_menu(), prevScreen = get_current_screen()/*, prevState = machine_state*/;
	
	//update_menu(currMenu);
	lcd_clear_screen();
	lcd_print_custom_message("SELECT MENU: ", 0, 0);
	lcd_print_custom_message("1: MAINTENANCE", 2, 0);
	lcd_print_custom_message("2: AUDIT", 1, 0);
	
	uint32_t timeout = millis();
	while(1){
		if(millis() - timeout > 60000){
			machineState = STATE_IDLE;
			lcd_print_error_msg("TIMEOUT!");
			ui_update(SCREEN_IDLE);
			DEBUG_PRINT("TIMEOUT!");
			return;
		}
		if(key == '1'){
			maintenance_loop();
			break;
		}
		else if(key == '2'){
			set_current_menu(MENU_AUDIT);
			break;
		}
		else if(key == 'C'){
			machineState = STATE_IDLE;
			ui_update(SCREEN_IDLE);
			return;
		}
		key = get_keypad_key();
	}
	
// 	while(key != 'C'){
// 		wdt_reset();
// 		currMenu = get_current_menu();
// 		key = get_keypad_key();
// 		if(key){Serial.println(key);}
// 		//else {continue;}
// 		
// 		if(key == '6'){
// 			if(++currMenu > MENU_AUDIT){currMenu = MENU_SET_PRICES;}
// 		}
// 		else if(key == '4'){
// 			if(++currMenu > MENU_AUDIT){currMenu = MENU_TIME;}
// 		}
// 		else if(key == '#'){
// 			lcd_print_error_msg("Please Wait...");
// 			vmc_eeprom_update_settings();
// 			machineState = STATE_IDLE;
// 			_delay_ms(100);
// 			ui_update(SCREEN_IDLE);
// 			break;
// 		}
		
// 		update_menu(currMenu);
// 		char isExit = 0;
// 		switch(currMenu){
// 			case MENU_SET_PRICES:{
// // 				if(key == '2'){settings.price_per_l++;}
// // 				else if(key == '8'){settings.price_per_l--;}
// // 				lcd_print_custom_message((String(settings.price_per_l)).c_str(), 1, LCD_COLUMNS - (String(settings.price_per_l)).length());
// 				isExit = set_prices_loop();
// 				break;
// 			}
// 			case MENU_SET_MIN:{
// // 				if(key == '2'){settings.minPrice++;}
// // 				else if(key == '8'){settings.minPrice--;}
// // 				lcd_print_custom_message((String(settings.minPrice)).c_str(), 1, LCD_COLUMNS - (String(settings.minPrice)).length());
// 				isExit = set_prices_loop();
// 				break;
// 			}
// 			case MENU_SET_MAX:{
// // 				if(key == '2'){settings.maxPrice++;}
// // 				else if(key == '8'){settings.maxPrice--;}
// // 				lcd_print_custom_message((String(settings.maxPrice)).c_str(), 1, LCD_COLUMNS - (String(settings.maxPrice)).length());
// 				isExit = set_prices_loop();
// 				break;
// 			}
// 			case MENU_CALIBRATE:{
// 				if(key == 'D'){
// 					set_calibrate_menu(CALIBRATE_SET_ML);
// 					calibration_loop();
// 				}
// 				break;
// 			}
// 			case MENU_TIME:{
// 				DEBUG_PRINT("Time Menu");
// 				time_menu_loop();
// 				set_current_menu(MENU_SET_PRICES);
// 				DEBUG_PRINT("Exit");
// 				break;
// 			}
// 			case MENU_AUDIT:{
// 				DEBUG_PRINT("Audit Menu");
// 				audit_menu_loop();
// 				machineState = STATE_IDLE;
// 				ui_update(SCREEN_IDLE);
// 				DEBUG_PRINT("Exit");
// 				return;
// 			}
// 		}
// 		_delay_ms(50);
// 		if(isExit){
// 			lcd_print_error_msg("Please Wait...");
// 			vmc_eeprom_update_settings();
// 			machineState = STATE_IDLE;
// 			_delay_ms(100);
// 			ui_update(SCREEN_IDLE);			
// 		}
// 	}
	machineState = STATE_IDLE;
	ui_update(SCREEN_IDLE);
}

void init_settings()
{
	vmc_set_price(eeprom_read_word((uint16_t*)PRICE_ADDRESS));
	vmc_set_min_price(eeprom_read_word((uint16_t*)MIN_PRICE_ADDRESS));
	vmc_set_max_price(eeprom_read_word((uint16_t*)MAX_PRICE_ADDRESS));
	set_pulses_per_litre(eeprom_read_word((uint16_t*)PULSE_ADDRESS));
}

void vmc_eeprom_update_settings()
{
	eeprom_update_word((uint16_t*)PRICE_ADDRESS, get_unit_price());
	eeprom_update_word((uint16_t*)MIN_PRICE_ADDRESS, settings.minPrice);
	eeprom_update_word((uint16_t*)MAX_PRICE_ADDRESS, settings.maxPrice);
	eeprom_update_word((uint16_t*)PULSE_ADDRESS, get_pulses_per_litre());
}

void vmc_set_price(uint16_t _price)
{
	settings.price_per_l = _price;
}

void vmc_set_min_price(uint16_t _minPrice)
{
	settings.minPrice = _minPrice;
}

void vmc_set_max_price(uint16_t _maxPrice)
{
	settings.maxPrice = _maxPrice;
}

uint16_t get_unit_price()
{
	return settings.price_per_l;
}

uint16_t get_price()
{
	return input_price;
}