#ifndef CASHLESS_H
#define CASHLESS_H

#define DEBUG					1

// #define MDB_AUDIT_DEVICE		1
// #define IS_DEX_ENABLED			1
// #define MPESA					1
// #define IS_OTA_CAPABLE			1
// #define IS_BLUETOOTH_ENABLED	1

#include <Arduino.h>
#include <avr/wdt.h>
#include <system.h>
#include <EEPROM.h>

#include "network.h"
#include "mqtt.h"
#include "DebugUtils.h"

#ifdef MDB_AUDIT_DEVICE
#include "mdb.h"
#include "FTL.h"
#endif

#ifdef IS_DEX_ENABLED
#include "dex.h"
#endif

#ifdef IS_BLUETOOTH_ENABLED
#include "bt.h"
#endif

#ifdef IS_OTA_CAPABLE
#include "ota.h"
#include "http.h"
#include "m95_spi.h"
#endif

#endif