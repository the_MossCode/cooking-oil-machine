/*
 * mqtt.cpp
 *
 * Created: 21/01/2019 11:30:06
 *  Author: Collo
 */ 

#include "cashless.h"
#include <ArduinoJson.h>
#include <MemoryFree.h>

MQTTClient mqtt(256);

bool mqtt_connect(){
	wdt_reset();
	
	int mqttConnectionAttempts=0;
	
	bool stateWritten = false;
	if (!mqtt.connected()){
		wdt_reset();
		DEBUG_PRINT("[WAIT] MQTT");
		
		if(!modem.isNetworkConnected()){
			//wdt_disable();
			modem.restart();
			DEBUG_PRINT("Waiting for Network");
			gprs_connect();
		}
		
		wdt_disable();
		while (!mqtt.connect(IMEI.c_str(), MQTT_USER, MQTT_PASS)) {
			DEBUG_PRINT(".");
			if(mqttConnectionAttempts++ > 10){
				DEBUG_PRINT("REBOOT");
				
				#ifdef MDB_AUDIT_DEVICE
				write_state_to_eeprom(get_user_funds());
				#endif
				
				wdt_enable(WDTO_120MS);
				while(1){;}
			}
			delay(1000);
		}
		mqttConnectionAttempts=0;
		DEBUG_PRINT("[OK] MQTT");
		
		#ifdef MDB_AUDIT_DEVICE
		if(get_cd_state() <= CD_DISABLED){
			delay(1000);
		}
		#endif
		
		wdt_enable(WDTO_8S);
		delay(1000);
	}
	//CONNECTION_STATE = CONNECTION_STATE_MQTT;
	//clear_mdb_rx_buffer();
	return true;
}

bool mqtt_subscribe()
{
	wdt_reset();
	if (!mqtt.connected()) {
		_delay_ms(200);
		mqtt_connect();
	}
	mqtt.subscribe("v/p/" + IMEI, MQTT_QOS);
	mqtt_event_subscribe();
	return true;
}

void mqtt_event_monit()
{
	wdt_reset();
	unsigned long stopwatchStart = millis();
	StaticJsonBuffer<128> monitBuffer;
	char payload[128];

	DEBUG_PRINT("EVENT MONITORING");
	
	if (!mqtt.connected()) {
		_delay_ms(200);
		mqtt_connect();
	}
	JsonObject& root = monitBuffer.createObject();
	root["rss"] = modem.getSignalQuality();
	root["ms"] = millis();
	root["ev"] = "check";
	
	#ifdef MDB_AUDIT_DEVICE
	root["s"] = get_cd_state();
	#endif
	
	root["mm"] = getFreeMemory();
	root.printTo(payload);
	
	wdt_reset();
	
	mqtt.publish("v/s/" + IMEI, payload, true, 1);
	DEBUG_PRINT(payload);
	
	unsigned long stopwatchEnd = millis();
	unsigned long elapsed = stopwatchEnd - stopwatchStart;
	DEBUG_PRINT(elapsed);
}

void mqtt_event_subscribe()
{
	wdt_reset();
	StaticJsonBuffer<200> monitBuffer;
	char payload[128];

	JsonObject& root = monitBuffer.createObject();
	root["rss"]		= modem.getSignalQuality();
	root["ms"]		= millis();
	root["ev"]		= "subscribe";
	
	#ifdef IS_OTA_CAPABLE
	root["ver"]		= verNo; 
	#endif
	
	root["mm"]		= getFreeMemory();
	
	root.printTo(payload);
	DEBUG_PRINT(payload);
	mqtt.publish("v/s/" + IMEI, payload, true, 1);
}

#ifdef MPESA
void mqtt_payment_received(const char* uuid, const char* s)
{
	wdt_reset();
	char payload[128];
	StaticJsonBuffer<200> checkerBuffer;

	JsonObject& checker = checkerBuffer.createObject();
	checker["u"] = uuid;
	checker["s"] = s;
	checker["ms"] = millis();
	checker["ev"] = "prec";
	checker["mm"] = getFreeMemory();
	checker.printTo(payload);
	mqtt.publish("v/p/" + IMEI + "/status", payload, true, 1);
}
#endif

void mqtt_product_dropped(const char* uuid, uint16_t product, uint16_t price)
{
	wdt_reset();
	char payload[128];
	StaticJsonBuffer<200> droppedBuffer;

	JsonObject& dropped = droppedBuffer.createObject();
	if(String(uuid).length() > 0){dropped["u"] = uuid;}
	dropped["g"] = product;
	dropped["p"] = price;
	
	#ifdef	MDB_AUDIT_DEVICE
	dropped["bal"] = get_user_funds(); 
	#endif
	
	dropped["rss"] = modem.getSignalQuality();
	dropped["ms"] = millis();
	dropped["ev"] = "drop";
	dropped["mm"] = getFreeMemory();
	dropped.printTo(payload);
	
	DEBUG_PRINT(payload);
	mqtt_connect();
	mqtt.publish("v/s/" + IMEI, payload, true, 1);
}

void messageReceived(String &topic, String &payload)
{
	wdt_reset();
	DEBUG_PRINT("\nIncoming: " + topic + " - " + payload);

	StaticJsonBuffer<256> jsonBuffer;
	JsonObject& messageFromServer = jsonBuffer.parse(payload);

	wdt_reset();
	/**
	* @TODO This is shit and has to be fixed asap
	*/
	JsonVariant configuration = messageFromServer["config"];
	if(configuration.success()) return;
	
	#ifdef MDB_AUDIT_DEVICE
	//****************************************
	//MDB
	wdt_reset();
	JsonVariant mdb = messageFromServer["mdb"];
	if(mdb.success()){
		if(mdb >= 1){
			isMDBEnabled = true;
		}
		else{
			isMDBEnabled = false;
		}
	}
	//*****************************************
	#endif
	
	//************************
	//OTA + BT
	JsonVariant serverEvent = messageFromServer["ev"];
	if(serverEvent.success()){
		if(serverEvent == "OTA"){
			
			#ifdef IS_OTA_CAPABLE
			wdt_reset();
			if(get_user_funds() > 0){
				Serial.println(get_user_funds());
				set_mdb_timeout(millis());	
			}
			String resource = messageFromServer["r"];
			//ota_get_server(resource);
			for(uint8_t i=0; i<resource.length(); ++i){
				otaResource[i] = resource.charAt(i);
			}
			
			String server = messageFromServer["ser"];
			
			if(server.length() > 0){
				strcpy(otaServer, server.c_str());
			}
			
			const char *version = messageFromServer["v"];
			
			if(verNo != String(version)){
				set_ota_status(true);
				Serial.println("OTA");
			}
			else{
				set_ota_status(false);
				DEBUG_PRINT("No Updates Available!");
			}
			#endif
			return;
		}
		//Bluetooth
		else if(serverEvent == "BT"){
			#ifdef IS_BLUETOOTH_ENABLED
			wdt_reset();
			DEBUG_PRINT("BT");
			int status = messageFromServer["s"];
			
			if(status == 0){
				set_bt_state(0);
				save_bt_state("");
				_delay_ms(50);
				bluetooth_event(0, 0);
				reboot();
				return;
			}
			else{
				wdt_reset();
				String tempMac = messageFromServer["mac"];
				tempMac = "38:A4:ED:82:7E:1B";
				tempMac.toLowerCase();
				set_mac_address(tempMac.c_str());
				set_bt_state(1);
				save_bt_state(tempMac);
				DEBUG_PRINT("Mac Address: "+tempMac);
				isMDBEnabled = false;
				reset_bluetooth_scan_interval();
				//return;
			}
			#endif
			return;		
		} 
		else if(serverEvent == "RESET"){
			reset_ack_event();
			#ifdef IS_BLUETOOTH_ENABLED
			set_bt_state(0);
			save_bt_state("");
			#endif
			
			#ifdef MDB_AUDIT_DEVICE
			set_user_funds(0);
			#endif
			reboot();
		}
		else if(serverEvent == "aud"){
			uint32_t timeout = millis();
			#ifdef IS_DEX_ENABLED
			unsigned char _error = establish_dex_status();
			while(_error != 0){
				if(millis() - timeout > 5000){
					mqtt_dex_event(_error);
				}
				
				_error = establish_dex_status();
			}
			#endif
			return;
		}
	}
	
	//***************************
	#ifdef MPESA
	delay(10);
	wdt_reset();
	const char *uuid = messageFromServer["u"];
	uint16_t price = messageFromServer["a"];
	mqtt_payment_received(uuid, "1");
	on_payment_received(price);
	set_product_uuid(uuid);
	
	_delay_ms(10);
	#endif
	//clear_mdb_rx_buffer();
	//mqtt_product_dropped(uuid, cdItemNumber);
}

void printKeyValue(const char *key, const char *value)
{
	DEBUG_PRINT(key);
	DEBUG_PRINT("  ");
	DEBUG_PRINT(value);
}

#ifdef MDB_AUDIT_DEVICE
void mqtt_dex_event(unsigned char _error)
{
	wdt_reset();
	StaticJsonBuffer<200> monitBuffer;
	char payload[128];

	JsonObject& root = monitBuffer.createObject();
	root["rss"]		= modem.getSignalQuality();
	root["ms"]		= millis();
	root["ev"]		= "audit";
	root["e"]		= _error;
	root["mm"]		= getFreeMemory();
	
	root.printTo(payload);
	DEBUG_PRINT(payload);
	mqtt.publish("v/s/" + IMEI, payload, true, 1);	
}

void bluetooth_event(uint8_t state, uint8_t _error)
{
	wdt_reset();
	
	char payLoad[128];
	
	StaticJsonBuffer<200> jsonBuffer;
	
	JsonObject& btBuffer = jsonBuffer.createObject();
	
	btBuffer["ev"]		= "BT";
	if(state <= 0){btBuffer["BTS"] = state;}
	else{btBuffer["s"]		= state;}
	btBuffer["e"]		= _error;
	btBuffer["mm"]		= getFreeMemory();
	btBuffer["rssi"]	= modem.getSignalQuality();
	
	btBuffer.printTo(payLoad);
	mqtt_connect();
	mqtt.publish("v/s/" + IMEI, payLoad, true, 1);
	
	DEBUG_PRINT(payLoad);
}

#endif

#ifdef IS_OTA_CAPABLE
void update_event(uint8_t status)
{
	wdt_reset();
	char payLoad[128];
	
	StaticJsonBuffer<200> jsonBuffer;
	
	JsonObject& otaResp = jsonBuffer.createObject();
	
	otaResp["ev"]	= "Update";
	otaResp["s"]	= status;
	otaResp["ver"]	= String(verNo);
	otaResp["mm"]	= getFreeMemory();
	otaResp["rss"]	= modem.getSignalQuality();
	
	otaResp.printTo(payLoad);
	DEBUG_PRINT(payLoad);
	
	mqtt_connect();
	
	mqtt.publish("v/s/" + IMEI, payLoad, true, 1);
	_delay_ms(100);
}
#endif

void reset_ack_event()
{
	wdt_reset();
	char payLoad[128];
	
	StaticJsonBuffer<200> jsonBuffer;
	
	JsonObject& ackData = jsonBuffer.createObject();
	
	ackData["ev"]	= "RESET";
	ackData["s"]	= "ACK";
	ackData["mm"]	= getFreeMemory();
	ackData["rss"]	= modem.getSignalQuality();
	
	ackData.printTo(payLoad);
	DEBUG_PRINT(payLoad);
	
	mqtt_connect();
	
	mqtt.publish("v/s/" + IMEI, payLoad, true, 1);
	_delay_ms(100);
}

void reboot()
{
	wdt_enable(WDTO_120MS);
	#ifdef MDB_AUDIT_DEVICE
	write_state_to_eeprom(get_user_funds());
	#endif
	DEBUG_PRINT("Reboot..");
	while(1){;}
}

void location_event(float lat, float lng)
{
	wdt_reset();
	char payload[128];
	StaticJsonBuffer<200> locationBuffer;

	JsonObject& location = locationBuffer.createObject();
	location["lat"] = lat;
	location["lng"] = lng;
	location["rss"] = modem.getSignalQuality();
	location["ms"] = millis();
	location["ev"] = "gps";
	location["mm"] = getFreeMemory();
	location.printTo(payload);

	if(!mqtt.connected()){
		_delay_ms(100);
		mqtt_connect();
	}
	mqtt.publish("v/s/" + IMEI, payload, true, 1);
	
	DEBUG_PRINT(payload);
}