/*
 * logger.cpp
 *
 * Created: 07/02/2019 15:05:53
 *  Author: Collo
 */ 
#include "cookingmachine.h"

void add_log(unsigned int _price)
{
	update_processor_time();
	update_day_log(_price);
	update_month_log();
}

void get_log(unsigned int *_sales, unsigned char *_date, unsigned char _day)
{
	update_processor_time();
	*_sales = get_day_log(_day, _date);
}

void update_day_log(unsigned int _price)
{
	DEBUG_PRINT("Adding Log: ");
	DEBUG_PRINT("Day : " + String(rtcDay) + " ,Price: " + String(_price));
	switch(rtcDay){
		case 1:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY1_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY1_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY1_ADDRESS+1), _price+p);
			break;
		}
		case 2:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY2_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY2_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY2_ADDRESS+1), _price+p);
			break;
		}
		case 3:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY3_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY3_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY3_ADDRESS+1), _price+p);
			break;
		}
		case 4:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY4_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY4_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY4_ADDRESS+1), _price+p);
			break;
		}
		case 5:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY5_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY5_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY5_ADDRESS+1), _price+p);
			break;
		}
		case 6:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY6_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY6_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY6_ADDRESS+1), _price+p);
			break;
		}
		case 7:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY7_ADDRESS+1));
			eeprom_update_byte((uint8_t*)LOGGER_DAY7_ADDRESS, rtcDate);
			eeprom_update_word((uint16_t*)(LOGGER_DAY7_ADDRESS+1), _price+p);
			break;
		}
		default:{
			break;
		}
	}
}

void update_month_log()
{
}

unsigned int get_day_log(unsigned char _day, unsigned char *_date)
{
	DEBUG_PRINT("DAY: "+String(_day));
	switch(_day){
		case 1:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY1_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY1_ADDRESS);
			return p;
		}
		case 2:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY2_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY2_ADDRESS);
			return p;
		}
		case 3:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY3_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY3_ADDRESS);
			return p;
		}
		case 4:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY4_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY4_ADDRESS);
			return p;
		}
		case 5:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY5_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY5_ADDRESS);
			return p;
		}
		case 6:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY6_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY6_ADDRESS);
			return p;
		}
		case 7:{
			unsigned int p = eeprom_read_word((uint16_t*)(LOGGER_DAY7_ADDRESS+1));
			*_date = eeprom_read_byte(LOGGER_DAY7_ADDRESS);
			return p;
		}
		default:{
			return 0;
		}
	}
}