/*
 * CookingMachine.h
 *
 * Created: 03/12/2018 15:23:38
 *  Author: Collo
 */ 


#ifndef COOKINGMACHINE_H_
#define COOKINGMACHINE_H_

#define DEBUG					1

#define PRICE_ADDRESS			16//2 byte address for uint16t
#define MAX_PRICE_ADDRESS		18
#define	MIN_PRICE_ADDRESS		20
#define PULSE_ADDRESS			24

//Includes
#include <avr/wdt.h>
#include <util/delay.h>
#include <system.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include <MemoryFree.h>
#include <EEPROM.h>
#include "ui.h"
#include "sensors.h"
#include "main_loop.h"
#include "DebugUtils.h"
#include "pump.h"
#include "eeprom.h"
#include "RTC.h"
#include "menu.h"
#include "logger.h"
#include "sm_keypad.h"

extern unsigned int rtcYear;
extern unsigned char rtcMonth;
extern unsigned char rtcDate;
extern unsigned char rtcDay;
extern unsigned char rtcHour;
extern unsigned char rtcMinute;
extern unsigned char rtcSecond;

#endif /* COOKINGMACHINE_H_ */