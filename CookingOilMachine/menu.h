/*
 * menu.h
 *
 * Created: 07/02/2019 13:32:06
 *  Author: Collo
 */ 


#ifndef MENU_H_
#define MENU_H_

enum Menu{
	MENU_SET_PRICES, /*MENU_SET_MIN, MENU_SET_MAX,*/ MENU_CALIBRATE, MENU_TIME, MENU_AUDIT
};

enum SubMenu{
	CALIBRATE_TOP_LEVEL,CALIBRATE_SET_ML, CALIBRATE_DISPENSE, CALIBRATE_SET_PULSES
};

void set_current_menu(unsigned char);
unsigned char get_current_menu();
void update_menu(unsigned char);
void menu_handler();
void maintenance_loop();
void exit_menu();

void menu_set_prices(void);
void menu_set_min_price(void);
void menu_set_max_prices(void);
void menu_set_time(void);

void calibrate_menu_handler();
unsigned char get_calibrate_menu();
void set_calibrate_menu(unsigned char);

char set_prices_loop();
void calibration_loop();
void time_menu_loop();
bool is_menu_pin_input_ok();

static void update_price_settings(char, unsigned int, char);

//AUDIT
void audit_menu_loop();

#endif /* MENU_H_ */