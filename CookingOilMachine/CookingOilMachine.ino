/*
 * CookingOilMachine.ino
 *
 * Created: 12/3/2018 3:19:44 PM
 * Author: Collo
 */ 

#include "CookingMachine.h"
#include "cashless.h"

//TODO Error Flags


void setup()
{
	Serial.begin(115200);
	vmc_init();
	rtc_setup();
	rtc_set_year(19);
	_delay_ms(500);
	Serial.println(rtc_get_year());
	//start_comms();
	  /* add setup code here, setup code runs once when the processor starts */
}

void loop()
{
	main_loop();
	 /* add main program code here, this code starts again each time it ends */
}
