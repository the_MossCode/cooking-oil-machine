/*
 * ui.cpp
 *
 * Created: 03/12/2018 15:40:50
 * Author: Collo
 */ 
#include "cookingmachine.h"
#include "LiquidCrystal.h"

LiquidCrystal lcd(RS, EN, D4, D5, D6, D7);
screen uiScreen = SCREEN_IDLE;

void init_display()
{
	DEBUG_PRINT("Initialising LCD");
	lcd.begin(LCD_COLUMNS, LCD_ROWS);
	update_lcd_message("Initialising...", 0, 0);
	_delay_ms(100);
}

void ui_update(uint8_t _screen)
{
	wdt_reset();
	set_current_screen(_screen);
	switch(uiScreen){
		case SCREEN_IDLE:{
			lcd.clear();
			update_lcd_message("Please Make a       Selection", 0, 0);
			break;
		}
		case SCREEN_PRICE:{
			if(get_price() > 0){
				lcd.clear();
				String p = "";
				p += String(get_price());
				
				print_price_message(p.c_str());	
			}
			break;
		}
		case SCREEN_SELECTION:{
			double litres = get_ml(get_price());
			
			String q = String(litres, 2);
			String p = String(get_price());
			
			DEBUG_PRINT("Litres:" + q);
			DEBUG_PRINT("Price:" + p);
			
			lcd.clear();
			print_selection_message(p.c_str(), q.c_str());
			break;
		}
		case SCREEN_DISPENSING:{
			lcd.clear();
			update_lcd_message("Dispensing", 0, 0);
			update_lcd_message("Please Wait...", 1, 0);
			break;
		}
		case SCREEN_DISPENSE_STATUS:{
			break;
			//ToDo
		}
		case SCREEN_MENU:{
			menu_handler();
			break;
		}
		default:{break;}
	}
}

void lcd_print_error_msg(const char* _msg)
{
	wdt_reset();
	lcd.clear();
	lcd_write_char_array(_msg);
	_delay_ms(2000);
}

void lcd_print_custom_message(const char *_msg, uint8_t _row, uint8_t _column)
{
	update_lcd_message(_msg, _row, _column);
}

void lcd_clear_screen()
{
	lcd.clear();
}

void set_current_screen(uint8_t _uiScreen)
{
	uiScreen = _uiScreen;
}

uint8_t get_current_screen()
{
	return uiScreen;
}

static void update_lcd_message(const char *_message, uint8_t _row, uint8_t _column)
{
	//DEBUG_PRINT(String(_message));
	lcd.setCursor(_column, _row);
	lcd_write_char_array(_message);
}

static uint8_t get_message_length(const char* _message)
{
	uint8_t count = 0;
	while(*_message++){
		count++;
	}
	
	return count;
}

static void lcd_write_char_array(char *str)
{
	while(*str){
		lcd.write(*str++);
	}
}

static void print_price_message(const char *_price)
{
	update_lcd_message("Price:", 0, 0);
	update_lcd_message(_price, 1, (LCD_COLUMNS - get_message_length(_price)));
}

static void print_selection_message(const char *_price, const char *_ml)
{	
	print_price_message(_price);
	update_lcd_message("Litres:", 2, 0);
	update_lcd_message(_ml, 3, (LCD_COLUMNS - get_message_length(_ml)));
}

static float get_ml(uint16_t _price)
{
	wdt_reset();
	//ToDo Move To An appropriate Header
	uint16_t temp = get_unit_price();
	Serial.println("P/L: " + String(temp));
	
	float litres = (float)_price/(float)temp;
	Serial.println("L: " + String(litres));
	return (litres);
}

void lcd_update_time()
{
	wdt_reset();
	lcd_clear_screen();
	lcd_print_custom_message("Time:", 0, 0);
	String timeStr = "";
	timeStr += (String(rtcDate)+"/"+String(rtcMonth)+"/"+String(rtcYear));
	lcd_print_custom_message(timeStr.c_str(), 1, 0);
	timeStr = "";
	timeStr += (String(rtcHour)+":"+String(rtcMinute)+":"+String(rtcSecond)+",");
	lcd_print_custom_message(timeStr.c_str(), 2, 0);
	_delay_ms(20);
}

void lcd_audit_handler(unsigned char _day)
{
	unsigned int _sales = 0;
	unsigned char _date = 0;
	DEBUG_PRINT("GETTING LOGS");
	get_log(&_sales, &_date, _day);
	lcd_clear_screen();
	lcd_print_custom_message(String("DATE: " + String(_date)+"/"+String(rtc_get_month())+"/"+String(rtc_get_year())).c_str(), 0, 0);
	lcd_print_custom_message(String("Sales: " + String(_sales)).c_str(), 1, 0);
}

void lcd_update_price_menu(char priceMenu)
{
	if(priceMenu == 0){
		lcd_clear_screen();
		lcd_print_custom_message("SET PRICE:", 0, 0);
		lcd_print_custom_message((String(settings.price_per_l)).c_str(), 1, LCD_COLUMNS - (String(settings.price_per_l)).length());
	}
	else if(priceMenu == 1){
		lcd_clear_screen();
		lcd_print_custom_message("SET MIN PRICE:", 0, 0);
		lcd_print_custom_message((String(settings.minPrice)).c_str(), 1, LCD_COLUMNS - (String(settings.minPrice)).length());		
	}
	else if(priceMenu == 2){
		lcd_clear_screen();
		lcd_print_custom_message("SET MAX PRICE:", 0, 0);
		lcd_print_custom_message((String(settings.maxPrice)).c_str(), 1, LCD_COLUMNS - (String(settings.maxPrice)).length());		
	}
}