/*
 * pump.h
 *
 * Created: 16/01/2019 14:04:32
 *  Author: Collo
 */ 


#ifndef PUMP_H_
#define PUMP_H_

#define PUMP_PIN		50
#define SOLENOID_VALVE	51

void init_pump();
void start_pump();
void stop_pump();

#endif /* PUMP_H_ */