/*
 * mdbmqtt.h
 *
 * Created: 26/06/2018 16:44:38
 *  Author: Collo
 */ 


#ifndef MDBMQTT_H_
#define MDBMQTT_H_

#define MQTT_SERVER   "169.239.252.95"// "169.239.252.95" // 169.239.252.95 // 46.101.23.152
#define MQTT_PORT     1883 // 1883
#define MQTT_USER     "john" // john
#define MQTT_PASS     "john" // john
#define MQTT_QOS      0

#include <MQTTClient.h>

extern MQTTClient mqtt;


bool mqtt_connect(void);
bool mqtt_subscribe(void);
void mqtt_event_monit(void);
void mqtt_event_subscribe(void);

#ifdef MPESA
void mqtt_payment_received(const char* uuid, const char* s);
#endif

void mqtt_product_dropped(const char* uuid, uint16_t product, uint16_t price);
void messageReceived(String &topic, String &payload);
void printKeyValue(const char *key, const char *value);

#ifdef MDB_AUDIT_DEVICE
void mdb_event();
#endif

#ifdef IS_BLUETOOTH_ENABLED
void bluetooth_event(uint8_t, uint8_t);
#endif

#ifdef IS_DEX_ENABLED
void mqtt_dex_event(unsigned char);
#endif

#ifdef IS_OTA_CAPABLE
void update_event(uint8_t);
#endif

void reset_ack_event();
void location_event(float, float);
void reboot(void);

#endif /* MDBMQTT_H_ */